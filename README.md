### DATA ###
- 4 Equipments: Cardio, Free Weights, Strength Machines, Synrgy360
- Values recorded for three sessions: Morning, Afternoon and Evening
- The split for training and testing correspond to 75% and 25%

### PREPROCESSING ###
- We record multiple values during each session and take an average over them to get one reading in each session.
- The gym is closed during holidays such as Christmas, Thanksgiving and Memorial day. Hence, we do not record usage data during the holidays.
- Missing values were filled by taking an average of the five previous readings for that particular day and session.

### CODE ###
- LSTM code was taken from the code in this paper "A Deep Learning Model for Wireless Channel Quality Prediction" by J. D. Herath and A. Seetharam and A. Ramesh

### RUN ###
- Set the PYTHONPATH variable with the current directory:
	- export PYTHONPATH=$PYTHONPATH:/home/adita/Desktop/equipment_usage_prediction
- The parameters that you can choose to run LSTM are:
	- fileName: location for input file
	- input-dim: number of variables at each time step
	- inputCols: M (Morning), A (Afternoon), E (Evening), O (Month_of_year), S (In_Session or Academic_Term)
	- path: path for the output location
	- x-length
	- y-length
	- minEpoch: epochs
	- learningRate
	- hiddenLayer: number of hidden units
	- numLayers: number of stacked layers
- You can run LSTM:
	python model/seq2seq_unguided_LSTM.py --fileName Data/Synrgy.csv --input-dim 2 --inputCols MS --path Results/lstm --x-length 14 --y-length 7 --minEpoch 500 --learningRate 0.01 --hiddenLayer 10 --numLayers 1
